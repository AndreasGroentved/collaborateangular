import {AttributeService} from '../attribute.service';
import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {UserManagerService} from '../user-manager.service';
import {IKey} from '../IKey';

@Component({
  templateUrl: './attributes.component.html',
  styleUrls: ['./attributes.component.css'],
  selector: 'attributes-component',
})

export class AttributeComponent implements OnInit, IKey, OnDestroy {
  @Input() source: string;
  attributeAdder: boolean;
  attrName: string = '';
  attrVal: string = '';

  constructor(private userManager: UserManagerService, public attributeService: AttributeService) {
    this.attributeAdder = false;
  }

  ngOnInit() {
    if (this.source === 'account') {
      this.attributeAdder = true;
      this.attributeService.setAttributes(this.userManager.getCurrentUser().Attributes); //TODO temp
    } else if (this.source.startsWith('survey')) {
      this.attributeAdder = false;
    } else if (this.source === 'createSurvey') {
      this.attributeAdder = true;
      this.attributeService.setAttributes([]);
    } else {
      this.attributeAdder = true;
      this.attributeService.setAttributes([]);
    }
  }

  onKey(event: any) {
    if (event.target.value.length > 2) {
      this.getAttributeSuggestions(event.target.value);
    } else {
      this.attributeService.setSuggestions([]);
    }
  }

  getAttributeSuggestions(searchVal: string) {
    this.attributeService.getAttributeSuggestions(searchVal);
  }

  select(item): void {
    this.attrName = item;
  }

  ngOnDestroy() {
    this.attributeService.setSuggestions([]);
  }

  private addAttribute(): void {
    this.attributeService.addAttribute(this.attrName, this.attrVal);
  }

  private deleteAttribute(index: number): void {
    this.attributeService.getAttributes().splice(index, 1);
  }
}
