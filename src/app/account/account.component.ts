import {AttributeService} from '../attribute.service';
import {AfterViewInit, Component} from '@angular/core';
import {UserManagerService} from '../user-manager.service';
import {UserHolder} from '../user-holder';


@Component({
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css'],
  selector: 'account-component',
})

export class AccountComponent implements AfterViewInit {
  constructor(private userManager: UserManagerService, public userHolder: UserHolder, private attributeService: AttributeService) {

  }

  updateUser(): void {
    this.userManager.updateUserAttributes(this.attributeService.getAttributes());
    this.userManager.saveUser();
    this.userManager.updateUser();
  }

  ngAfterViewInit() {

  }
}
