import {Attribute} from './attribute';
import {UserDTO} from './user-dto';

export class User {


  constructor(public Name: string = '', public Gender: string = '', public Email: string = '', public Attributes: Attribute[] = []) {
  }

  setUser(userDTO: UserDTO): void {
    this.Name = userDTO.Name;
    this.Gender = userDTO.Gender;
    this.Email = userDTO.Email;
    this.Attributes = userDTO.Attributes;
  }

}
