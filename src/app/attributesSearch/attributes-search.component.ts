import {SearchAttribute} from '../data-objects/SearchAttribute';
import {AttributeService} from '../attribute.service';
import {Attribute} from '../data-objects/attribute';
import {Component, OnDestroy} from '@angular/core';
import {IKey} from '../IKey';

@Component({
  templateUrl: './attributes-search.component.html',
  styleUrls: ['./attributes-search.component.css'],
  selector: 'attributes-search-component',

})

export class AttributeSearchComponent implements IKey, OnDestroy {
  values: string[];
  type: string = '';
  isActive: boolean = true;
  must: boolean = false;
  attrName: string = '';
  minNum = 0;
  minVal: number = 0;
  maxVal: number = 0;
  boolVal: boolean = true;
  stringVal: string = '';
  attributeMap: Map<string, Number>;

  constructor(public attributeService: AttributeService) {
    this.values = [];
    this.values.push('String');
    this.values.push('Boolean');
    this.values.push('Number');
    this.type = 'Boolean';
    this.attributeService.setSearchAttributes([]);
    this.attributeMap = new Map();
    this.attributeMap['alle'] = 0;
  }

  changeNot(): void { //TODO vel ikke nødvendigt
    this.isActive = !this.isActive;
  }

  changeMust(): void {
    this.must = !this.must;
  }

  changeBoolVal(): void {
    this.boolVal = !this.boolVal;
  }

  onKey(event: any) {
    if (event.target.value.length > 2) {
      this.attributeService.getAttributeSuggestions(event.target.value);
    } else {
      this.attributeService.setSuggestions([]);
    }
  }

  select(item): void {
    this.attrName = item;
  }

  selectItem(item): void {
    this.type = item;
  }

  ngOnDestroy() {
    this.attributeService.setSuggestions([]);
  }

  addAttribute(): void {
    const tempAttr: SearchAttribute = new SearchAttribute();
    tempAttr.Attribute = new Attribute();

    switch (this.type) {
      case 'String':
        tempAttr.Attribute.setAttributeData(this.attrName, this.stringVal);

        if (this.attrName === '') {
          alert('Værdi påkrævet');
          return;
        }
        break;
      case 'Number':
        tempAttr.Attribute.setAttributeData(this.attrName, this.minVal);
        break;
      case 'Boolean':
        tempAttr.Attribute.setAttributeData(this.attrName, this.boolVal);
        break;
    }
    tempAttr.Frequency = this.minNum >= 1 ? this.minNum : -1;
    tempAttr.MinVal = this.minVal >= 0 ? this.minVal : -1;
    tempAttr.MaxVal = this.maxVal >= 0 ? this.maxVal : -1;
    tempAttr.Must = this.must;
    tempAttr.Not = this.type === 'String' ? this.isActive : false;
    this.attributeMap[tempAttr.Attribute.Name] = -1; //TODO tjek om nødvendig
    this.attributeService.addSearchAttribute(tempAttr);
    this.getAttributeCounts();
  }

  deleteAttribute(index: number): void {
    this.attributeService.getSearchAttributes().splice(index, 1);
    this.getAttributeCounts();
  }

  getAttributeCounts() {
    this.attributeService.getAttributeCounts().first().subscribe((attrMap) => {
      this.attributeMap = attrMap;
    });
  }

}
