import {Message} from './data-objects/message';

export class InstructionServer {

  constructor(public Email: string, public Message: Object, public CallId: string) {
  }

  static getInstructionServer(email: string, methodName: string, argument: Object, callId: string = ''): InstructionServer {
    return new InstructionServer(email, new Message(methodName, argument), callId);
  }
}
