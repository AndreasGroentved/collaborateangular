import {SecurityService} from './security';
import {InstructionServer} from './instruction-server';
import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs/Rx';
import {InstructionClient} from './instruction-client';
import {ISocketService} from './ISocketService';
import {EncryptionHandler} from './encryption_handler';

const SOCKET_URL = 'ws://localhost:56411/ws';

@Injectable()
export class SocketManagerService implements ISocketService {
  private ws: WebSocket;
  private socketStateObserver: Subject<Boolean>;
  private requestMap: Map<string, Subject<InstructionClient>>;

  constructor(private encryption: EncryptionHandler) {
    this.create(SOCKET_URL);
    this.requestMap = new Map<string, Subject<InstructionClient>>();
    this.socketStateObserver = new Subject();
    // TODO pushing af beskeder om ny survey man deltager i
  }

  private create(url): void {
    console.log('create');
    this.ws = new WebSocket(url);
    this.ws.onmessage = (msg) => {
      console.log(msg.data);
      const instructionClient = this.encryption.decrypt(msg.data);
      this.handleCallBack(instructionClient);

      if (!this.socketStateObserver.isEmpty()) {
        this.socketStateObserver.next(true); // TODO isEmpty virker måske ikke som en bool
      }
    };
    this.ws.onerror = () => {
      console.log('*** Socket Error ***');
    };
    this.ws.onclose = () => {
      console.log('*** Socket closing ***');
    };
    this.ws.onopen = () => {
      console.log('*** Socket open***');
      this.socketStateObserver.next(true);
    };
    this.ws.close.bind(this.ws);
  }

  private handleCallBack(instructionClient: InstructionClient) {
    if (instructionClient === null) { //Sker kun hvis server sender ugyldigt data
    } else {
      if (this.requestMap.has(instructionClient.CallId)) {
        this.requestMap.get(instructionClient.CallId).next(instructionClient);
        this.answerReceived(instructionClient.CallId);
      }
    }
  }

  private answerReceived(callId: string) {
    this.requestMap.delete(callId);
  }

  //TODO måske 2 metoder i stedet for
  public sendRequest(request: InstructionServer, key: string = ''): Observable<InstructionClient> {
    console.log(request);
    const callBack: Subject<InstructionClient> = this.setCallBack(request);
    if (this.ws.readyState === WebSocket.OPEN) {
      this.ws.send(this.encryption.encrypt(request, key));
    } else if (this.ws.readyState === WebSocket.CONNECTING) {
      this.socketStateObserver.first().subscribe(() => this.sendRequest(request, key));
      console.log('Socket not ready');
    } else if (this.ws.readyState === WebSocket.CLOSED) {
      this.create(SOCKET_URL);
      console.log('recreate connection');
      this.socketStateObserver.first().subscribe(() => this.sendRequest(request, key));
    } else if (this.ws.readyState === WebSocket.CLOSING) {
      // TODO Figure out what to do here
    }
    return callBack;
  }

  private setCallBack(request: InstructionServer): Subject<InstructionClient> {
    let callBack: Subject<InstructionClient>;
    if (request.CallId === '') request.CallId = SecurityService.getRandomId();

    if (this.requestMap.has(request.CallId)) callBack = this.requestMap.get(request.CallId);
    else {
      callBack = new Subject();
      this.requestMap.set(request.CallId, callBack);
    }
    return callBack;
  }
}
