import {UserHolder} from './user-holder';
import {SocketManagerService} from './socket-manager.service';
import {UserSurveyService} from './user-survey.service';
import {AttributeService} from './attribute.service';
import {AuthGuardLoggedOut} from './auth-guard-logged-out.service';
import {AuthGuardLoggedIn} from './auth-guard-logged-in.service';
import {UserManagerService} from './user-manager.service';
import {SurveyStatusComponent} from './survey-status/survey-status.component';
import {AttributeSearchComponent} from './attributesSearch/attributes-search.component';
import {SurveyComponent} from './survey/survey.component';
import {CreateSurveyComponent} from './create-survey/create-survey.component';
import {AccountComponent} from './account/account.component';
import {AttributeComponent} from './attributes/attributes.component';
import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {AppRoutingModule} from './app-routing/app-routing.module';
import {AppComponent} from './app.component';
import {LoginComponent} from './login/login.component';
import {SignupComponent} from './signup/signup.component';
import {LoggedInComponent} from './logged-in/logged-in.component';
import {LeftMenuComponent} from './left-menu/left-menu.component';
import {ISocketService} from './ISocketService';
import {EncryptionHandler} from './encryption_handler';
import {SecurityService} from './security';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SignupComponent,
    LoggedInComponent,
    LeftMenuComponent,
    AttributeComponent,
    AccountComponent,
    CreateSurveyComponent,
    SurveyComponent,
    AttributeSearchComponent,
    SurveyStatusComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
  ],
  providers: [UserManagerService, AuthGuardLoggedIn, SecurityService, AuthGuardLoggedOut, AttributeService, UserSurveyService, UserHolder, EncryptionHandler
    , {provide: ISocketService, useClass: SocketManagerService}],
  bootstrap: [AppComponent]
})
export class AppModule {
}
