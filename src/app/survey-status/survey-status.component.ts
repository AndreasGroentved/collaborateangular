import {UserSurveyService} from '../user-survey.service';
import {Component} from '@angular/core';

@Component({
  templateUrl: './survey-status.component.html',
  styleUrls: ['./survey-status.component.css'],
  selector: 'survey-status-component',

})

export class SurveyStatusComponent {
  constructor(public surveyService: UserSurveyService) {
  }
}
