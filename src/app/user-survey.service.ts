import {Injectable} from '@angular/core';
import {User} from './data-objects/user';
import {Subject} from 'rxjs';
import {SurveySearch} from './data-objects/SurveySearch';
import {Login} from './data-objects/login';
import {InstructionServer} from './instruction-server';
import {Message} from './data-objects/message';
import {ISocketService} from './ISocketService';
import {Observable} from 'rxjs/Observable';
import {AttributeService} from './attribute.service';
import {UserHolder} from './user-holder';


@Injectable()
export class UserSurveyService {
  surveysIn: SurveySearch[]; //TODO overvej map - komponent kan finde ud af, ud fra brugerid, om brugeren har oprettet/deltager
  surveysCreated: SurveySearch[];
  currentSurvey: SurveySearch;
  currentSurveyId: string = '-1';
  surveySearchResult: SurveySearch;

  constructor(private userHolder: UserHolder, private attributeService: AttributeService, private socketService: ISocketService) {
    this.surveysIn = [];
    this.surveysCreated = [];
    this.currentSurvey = new SurveySearch();
  }

  public setSurveySearchResult(s: SurveySearch): void {
    this.surveySearchResult = s;
  }

  public getSurveySearchResult(): SurveySearch {
    return this.surveySearchResult;
  }

  updateSurveys(surveys: SurveySearch[]) {
    const user: User = this.userHolder.currentUser;
    this.surveysCreated = [];
    this.surveysIn = [];
    for (const survey of surveys) {
      if (survey.Id === this.currentSurveyId) this.currentSurvey = survey;
      if (survey.User === user.Email) this.surveysCreated.push(survey);
      else this.surveysIn.push(survey);
      if (this.currentSurvey.Id === survey.Id) this.currentSurvey = survey;
    }
  }

  getSurvey(id: string) {
    for (const survey of this.surveysIn) {
      if (id === survey.Id) return survey;
    }
    for (const survey of this.surveysCreated) {
      if (id === survey.Id) return survey;
    }
    const s: SurveySearch = new SurveySearch();
    s.Id = id;
    return s;
  }

  createUserSurvey(fraDate: string, tilDate: string, antalBrugere: number, beskrivelse: string): Observable<string> {
    const login: Login = this.userHolder.login;
    const surveySearch: SurveySearch = new SurveySearch(this.attributeService.getSearchAttributes(), login.Email,
      new Date(fraDate), new Date(tilDate), antalBrugere, new Date(), beskrivelse);
    //TODO createSurvey=> createSureveyRequest
    const instructionServer = InstructionServer.getInstructionServer(this.userHolder.login.Email, 'CreateUserSurvey', surveySearch);
    return this.surveySearchFromServer(instructionServer);
  }

  private surveySearchFromServer(instructionServer: InstructionServer): Observable<string> {
    const subject: Subject<string> = new Subject();
    this.socketService.sendRequest(instructionServer, this.userHolder.login.Password).first().subscribe(instruction => {
      const message: Message = instruction.Argument as Message;
      const id: string = message.Argument as string;
      subject.next(id);
    });
    return subject;
  }

  public getSurveys(): void {
    const login: Login = this.userHolder.login;
    const instructionServer: InstructionServer = InstructionServer.getInstructionServer(login.Email, 'GetUserSurveys', {});
    this.getUserSurveysFromServer(instructionServer, login);
  }

  private getUserSurveysFromServer(instructionServer: InstructionServer, login: Login) {
    this.socketService.sendRequest(instructionServer, login.Password).first().subscribe(instruction => {
      const message: Message = instruction.Argument as Message;
      const surveys: SurveySearch[] = message.Argument as SurveySearch[];
      this.updateSurveys(surveys);
    });
  }

  updateUserSurvey(isActive: boolean) {
    const s: SurveySearch = this.currentSurvey;
    s.ParticipationFlag = isActive;
    const instructionServer: InstructionServer = InstructionServer.getInstructionServer(this.userHolder.login.Email, 'UpdateUserSurvey', s);
    this.socketService.sendRequest(instructionServer, this.userHolder.login.Password).first().subscribe(instruction => {
      const message: Message = instruction.Argument as Message;
      const updated: boolean = message.Argument as boolean;
      this.getSurveys();
    });
  }

}
