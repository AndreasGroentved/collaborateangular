import {SearchAttribute} from './SearchAttribute';

export class SurveySearch {

  constructor(public Attributes: SearchAttribute[] = [], public User: string = '', public FromDate: Date = new Date(),
              public ToDate: Date = new Date(), public NumberOfUsers: number = -1, public CreationDate: Date = new Date(),
              public Description: string = 'invalid survey', public NumberConfirmed: number = -1, public ParticipationFlag: boolean = false,
              public NumberDenied: number = -1, public Id: string = '-1') {
  }
}
