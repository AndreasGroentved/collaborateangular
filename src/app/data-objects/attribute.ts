import {Types} from './type';

export class Attribute {

  constructor(public Name = 'def', public Type = Types.STRING, public Value: any = '') {
  }

  isFloat(n): boolean { //TODO util
    return Number(n) === n && n % 1 !== 0;
  }

  public setAttributeData(Name: string, Value: any) {
    this.Value = Value;
    this.Name = Name;


    if (Value === true || Value === false) {
      this.Type = Types.BOOLEAN;
    }
    else if ((!isNaN(Number(Value)) || Value === 0) && Value !== '') {
      if (this.isFloat(Value)) {
        this.Type = Types.FLOAT;
      } else {
        this.Type = Types.INTEGER;
      }
    } else {
      this.Type = Types.STRING;
    }

  }
}
