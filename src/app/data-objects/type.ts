export enum Types {
  INTEGER,
  FLOAT,
  BOOLEAN,
  STRING
}