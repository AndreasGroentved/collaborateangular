import {Router} from '@angular/router';
import {AttributeService} from '../attribute.service';
import {Component} from '@angular/core';
import {UserManagerService} from '../user-manager.service';

@Component({
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css'],
  providers: []
})
export class SignupComponent {
  gender: string = '';
  name: string = '';
  age: number = NaN;
  password1: string = '';
  password2: string = '';
  email: string = '';

  constructor(private userManager: UserManagerService, private attributeService: AttributeService, private router: Router) {
    this.attributeService.setAttributes([]);
  }

  signup(): void {
    // First check if any of the textboxes are missing input
    const validation: boolean = this.validation();
    if (!validation) return;
    this.userManager.signUp(this.gender, this.name, this.age, this.password1, this.email).first().subscribe(() => {
      this.router.navigateByUrl('');
    });
  }

  private validation() {
    let shouldCreateUser: boolean = true;
    this.validateUser().forEach((value: boolean, key: string) => {
      if (!value) {
        shouldCreateUser = false;
        alert(key); //TODO mere passende...
      }
    });
    return shouldCreateUser;
  }

  private validateUser(): Map<string, boolean> {
    const boolMap: Map<string, boolean> = new Map();
    boolMap.set('name is empty', this.validateName());
    boolMap.set('gender is not selected', this.validateGender());
    boolMap.set('password 1 is empty', this.validatePass1());
    boolMap.set('password 2 is empty', this.validatePass2());
    boolMap.set('age is empty', this.validateAge());
    boolMap.set('email is invalid', this.validateEmail(this.email));
    boolMap.set('passwords are not equal', this.validatePassEquality());
    return boolMap;
  }

  private validateAge() {
    return !isNaN(this.age) && this.age !== null;
  }

  private validatePass1() {
    return this.password1 !== '';
  }

  private validatePass2() {
    return this.password2 !== '';
  }

  private validateName(): boolean {
    return this.name !== '';
  }

  private validateGender(): boolean {
    return this.gender !== '';
  }

  private validateEmail(email): boolean {
    if (email === '') return false;

    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  private validatePassEquality(): boolean {
    return this.password1 === this.password2;
  }
}
