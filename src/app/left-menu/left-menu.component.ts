import {Router} from '@angular/router';
import {Component} from '@angular/core';
import {UserManagerService} from '../user-manager.service';

@Component({
  templateUrl: './left-menu.component.html',
  styleUrls: ['./left-menu.component.css'],
  selector: 'left-menu-component',
})

export class LeftMenuComponent {
  constructor(private userManager: UserManagerService, private router: Router) {
  }

  logOut(): void {
    this.userManager.userLoggedOut();
    this.router.navigateByUrl('/');
  }

}
