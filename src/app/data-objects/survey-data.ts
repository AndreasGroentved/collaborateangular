import {Attribute} from './attribute';
import {Injectable} from '@angular/core';

@Injectable()
export class SurveyData {

  constructor(public Id: String = '-1', public Attributes: Attribute[] = [], public User: String = '-1', //TODO faktisk user
              public FromDate: Date = new Date(), public ToDate: Date = new Date(),
              public NumberOfUsers: number = -1, public CreationDate: Date = new Date(),
              public ParticipationFlag: boolean = false, public NumberConfirmed: number = -1,
              public NumberDenied: number = -1, public Description: string = 'invalid survey') {
  }
}
