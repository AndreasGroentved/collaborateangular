import {Injectable} from '@angular/core';
import {User} from './data-objects/user';
import {Login} from './data-objects/login';

@Injectable()
export class UserHolder {

  public currentUser: User = new User();
  public login: Login = new Login();

  constructor() {
  }

  public isLoggedIn(): boolean {
    return this.currentUser !== null && this.currentUser.Name !== '';
  }
}
