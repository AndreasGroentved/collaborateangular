import { CollaborateAngularPage } from './app.po';

describe('collaborate-angular App', () => {
  let page: CollaborateAngularPage;

  beforeEach(() => {
    page = new CollaborateAngularPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
