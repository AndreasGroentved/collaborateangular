import {Router} from '@angular/router';
import {Component, ViewEncapsulation} from '@angular/core';
import {UserManagerService} from '../user-manager.service';

@Component({
  selector: 'login-component',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  encapsulation: ViewEncapsulation.None
})

export class LoginComponent {
  email: string;
  password: string;

  constructor(private userManager: UserManagerService, private router: Router) {
  }

  sendLoginRequest() {
    this.userManager.sendLoginRequest(this.email, this.password).first().subscribe(success => {
      if (success) this.router.navigateByUrl('/account');
    });
  }

  logoutUser(): void {
    this.userManager.userLoggedOut();
  }
}
