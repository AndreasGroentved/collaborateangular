import {Injectable} from '@angular/core';
import {UserManagerService} from './user-manager.service';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';

@Injectable()
export class AuthGuardLoggedIn implements CanActivate {

  constructor(private userManager: UserManagerService, private router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const match: string = this.router.url.replace('/\// ', '');
    if (!this.userManager.isLoggedIn()) {
      this.router.navigate(['/login']);
      return false;
    }
    if (route.url.toString() === '') {
      this.router.navigate(['/account']);
      return false;
    }
    return true;
  }


}
