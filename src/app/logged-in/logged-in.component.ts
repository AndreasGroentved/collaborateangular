import {AfterViewInit, Component} from '@angular/core';
import {NavigationEnd, Router} from '@angular/router';

@Component({
  selector: 'logged-in-component',
  templateUrl: './logged-in.component.html',
  styleUrls: ['./logged-in.component.css'],
})

export class LoggedInComponent implements AfterViewInit {

  active: string;

  constructor(private router: Router) {
    if (this.router.url === '') {
      this.active = 'account';
      this.router.navigateByUrl('/account');
    } else {
      this.handleIsActive(this.router.url);
    }
  }

  ngAfterViewInit() {
    this.router.events.subscribe((val) => {
      if (val instanceof NavigationEnd) {
        this.handleIsActive(val.url);
      }
    });
  }

  private handleIsActive(url) {
    if (url === '/account') this.active = 'account';
    else if (url.startsWith('/surveys')) this.active = 'surveys';
    else if (url.includes('/findUsers')) this.active = 'findUsers';
  }

}
