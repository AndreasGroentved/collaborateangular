import {Router} from '@angular/router';
import {Component} from '@angular/core';
import {UserSurveyService} from '../user-survey.service';

@Component({
  templateUrl: './create-survey.component.html',
  styleUrls: ['./create-survey.component.css'],
  selector: 'create-survey-component',
})

export class CreateSurveyComponent {

  fraDate: string = '';
  tilDate: string = '';
  antalBrugere: number = 0;
  beskrivelse: string = '';

  constructor(private router: Router, private userSurveyService: UserSurveyService) {
    this.fraDate = new Date().toISOString().slice(0, 16);
    this.tilDate = new Date().toISOString().slice(0, 16);
  }

  createUserSurvey() {
    this.userSurveyService.createUserSurvey(this.fraDate, this.tilDate, this.antalBrugere, this.beskrivelse).first().subscribe((id) => {
      if (id !== '-1') {
        const url: string = '/surveys/' + id;
        this.router.navigate([url]);
      }
    });
  }


}
