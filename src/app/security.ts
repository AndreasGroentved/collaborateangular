import {UserHolder} from './user-holder';
import {InstructionClient} from './instruction-client';
import {InstructionServer} from './instruction-server';
import {Injectable} from '@angular/core';

declare let CryptoJS: any;
declare let RSAKey: any;

@Injectable()
export class SecurityService {
  private rsaKey;

  constructor(private userHolder: UserHolder) {
    this.rsaKey = new RSAKey();
    this.rsaKey.setPublic(`00bc281cfe8139c6617cdeb64335773a6d7a08fc2a4dab5d04a2f5ed27f2d534a5c6bf960f3ce1bb0e98f7fad610b48a712e4d61efce001167c4358cc75682f03810f4756df3d417694b12b53b3f480e23ad345f3d188788abe89e2b8225778ac2732cb4ce11ee95f42db114575ff983782fe1111fe516cf2876d6474f3fb876db`, '10001');
  }

  public encryptInstructionServerAES(toEncrypt: InstructionServer): void {
    console.log('encrypt AES');
    let key = this.userHolder.login.Password;
    key = CryptoJS.enc.Base64.parse(key);
    const iv = CryptoJS.enc.Utf8.parse('8080808080808080');
    const jsonString = CryptoJS.enc.Utf8.parse(JSON.stringify(toEncrypt.Message));
    const encrypted = CryptoJS.AES.encrypt(jsonString, key,
      {
        keySize: 128 / 8, iv: iv,
        mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7
      }
    );
    toEncrypt.Message = encrypted.toString();
  }


  public encryptStringAES(toEncrypt: string, key: string): string { //TODO få styr på, hvem der krypterer...
    key = CryptoJS.enc.Base64.parse(key);
    const iv = CryptoJS.enc.Utf8.parse('8080808080808080');
    const jsonString = CryptoJS.enc.Utf8.parse(toEncrypt);
    const encrypted = CryptoJS.AES.encrypt(jsonString, key,
      {
        keySize: 128 / 8, iv: iv,
        mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7
      }
    );

    return encrypted.toString();
  }

  public decryptAES(toDecrypt: InstructionClient): void {
    let key = this.userHolder.login.Password;
    const iv = CryptoJS.enc.Utf8.parse('8080808080808080');
    key = CryptoJS.enc.Base64.parse(key);
    console.log(toDecrypt);

    const test = CryptoJS.AES.decrypt(toDecrypt.Argument, key, {
      keySize: 128 / 8, iv: iv,
      mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7
    });
    toDecrypt.Argument = JSON.parse(test.toString(CryptoJS.enc.Utf8)); //as Message;
  }

  public encryptRSA(toEncrypt: string): string {
    const a = new RSAKey();
    a.setPublic('00bc281cfe8139c6617cdeb64335773a6d7a08fc2a4dab5d04a2f5ed27f2d534a5c6bf960f3ce1bb0e98f7fad610b48a712e4d61efce001167c4358cc75682f03810f4756df3d417694b12b53b3f480e23ad345f3d188788abe89e2b8225778ac2732cb4ce11ee95f42db114575ff983782fe1111fe516cf2876d6474f3fb876db', '10001');
    return a.encrypt(toEncrypt);
  }

  public hashString(toHash: string): string {
    return CryptoJS.SHA256(toHash).toString(CryptoJS.enc.Base64);
  }

  public static getRandomId(): string {
    let text = '';
    const possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    for (let i = 0; i < 5; i++) {
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return text;
  }
}
