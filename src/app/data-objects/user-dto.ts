import {Attribute} from './attribute';

//TODO måske bare interface?

export class UserDTO {

  constructor(public Name: string = '', public Email: string = '', public Gender: string = '', public Attributes: Attribute[] = []) {
  }
}
