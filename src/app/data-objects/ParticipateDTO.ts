export class ParticipateDTO {
  constructor(public Participates: boolean = false, public Email: string = '', public HasAnswered: boolean = false) {
  }
}
