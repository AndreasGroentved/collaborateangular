import {Injectable} from '@angular/core';
import {SecurityService} from './security';
import {InstructionServer} from './instruction-server';
import {User} from './data-objects/user';
import {Login} from './data-objects/login';
import {Registration} from './data-objects/Registration';
import {Message} from './data-objects/message';
import {UserHolder} from './user-holder';
import {InstructionClient} from './instruction-client';

@Injectable()
export class EncryptionHandler {

  constructor(private security: SecurityService, private userHolder: UserHolder) {
  }

  public decrypt(data): InstructionClient {
    const instructionClient: InstructionClient = JSON.parse(data) as InstructionClient;
    const encrypted: boolean = !(instructionClient.Argument instanceof Object);
    if (encrypted) this.security.decryptAES(instructionClient);
    return instructionClient;
  }

  public encrypt(request: InstructionServer, key: string = ''): string {
    if (this.shouldAES(key)) this.aesEncrypt(request);
    else if (this.shouldRSAEncrypt(key)) this.rsaEncrypt(request);

    console.log(request);
    return JSON.stringify(request);
  }

  private rsaEncrypt(request: InstructionServer) {//Lille smule kluntet
    const registration: Registration = (request.Message as Message).Argument as Registration;
    registration.Password = this.security.hashString(registration.Password);
    this.userHolder.login = new Login((registration.User as User).Email, registration.Password);
    registration.User = this.security.encryptStringAES(JSON.stringify(registration.User), registration.Password);
    registration.Password = this.security.encryptRSA(registration.Password);
    console.log(registration);
  }

  private aesEncrypt(request: InstructionServer) {
    this.security.encryptInstructionServerAES(request);
    request.Email = this.security.encryptRSA(request.Email);
  }

  private shouldRSAEncrypt(key: string) {
    return key === 'RSA';
  }

  private shouldAES(key: string):boolean {
    return key !== '' && key !== 'RSA';
  }
}
