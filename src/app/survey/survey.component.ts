import {UserHolder} from '../user-holder';
import {NavigationEnd, Router} from '@angular/router';
import {UserSurveyService} from '../user-survey.service';
import {AfterViewInit, Component} from '@angular/core';

@Component({
  templateUrl: './survey.component.html',
  styleUrls: ['./survey.component.css'],
  selector: 'survey-component'
})

export class SurveyComponent implements AfterViewInit {
  showSingle: boolean = false;
  isActive: boolean;

  constructor(public userSurveyService: UserSurveyService, private router: Router, public userHolder: UserHolder) {
    this.router.events.subscribe(v => {
      if (v instanceof NavigationEnd) this.handleRouterUrl((v as NavigationEnd).url);
    });
    this.handleRouterUrl(router.url);
  }

  changeNot(): void { //TODO navn
    this.isActive = !this.isActive;
  }

  update() {
    this.userSurveyService.updateUserSurvey(this.isActive);
  }


  private handleRouterUrl(url: string): void {
    if (url === '/surveys') {
      this.showSingle = false;
    } else if (url.includes('/surveys/')) {

      this.showSingle = true;
      const id: string = url.split('/').pop();
      this.userSurveyService.currentSurveyId = id;
      this.userSurveyService.currentSurvey = this.userSurveyService.getSurvey(id);
    }
  }

  ngAfterViewInit() {
    this.getSurveys();
  }

  private getSurveys(): void {
    this.userSurveyService.getSurveys();
  }
}
