import {InstructionClient} from './instruction-client';
import {Observable} from 'rxjs/Observable';
import {InstructionServer} from './instruction-server';

export abstract class ISocketService {
  abstract sendRequest(request: InstructionServer, key: string): Observable<InstructionClient>;
}
