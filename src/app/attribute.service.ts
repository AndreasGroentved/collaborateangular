import {SearchAttribute} from './data-objects/SearchAttribute';
import {AttributeSearch} from './data-objects/AttributeSearch';
import {Injectable} from '@angular/core';
import {Attribute} from './data-objects/attribute';
import {InstructionServer} from './instruction-server';
import {Message} from './data-objects/message';
import {ISocketService} from './ISocketService';
import {Observable} from 'rxjs/Observable';
import {Subject} from 'rxjs/Subject';
import {UserHolder} from './user-holder';


@Injectable()
export class AttributeService {
  attributes: Attribute[];
  suggestions: string[];
  apriori: Attribute[];
  searchAttributes: SearchAttribute[];

  constructor(private userHolder: UserHolder, private socketService: ISocketService) {
    this.attributes = [];
    this.suggestions = [];
    this.apriori = [];
    this.searchAttributes = [];
  }

  public addSearchAttribute(sA: SearchAttribute) {
    this.searchAttributes.push(sA);
  }

  public setSearchAttributes(s: SearchAttribute[]) {
    this.searchAttributes = s;
  }

  public getSearchAttributes(): SearchAttribute[] {
    return this.searchAttributes;
  }

  public updateSuggestions(attributeSearch: AttributeSearch) {
    this.suggestions = attributeSearch.CorrectionList.concat(attributeSearch.AutoCompleteList);
    this.suggestions = Array.from(new Set(this.suggestions));
  }


  public setApriori(attributes: Attribute[]) {
    this.apriori = attributes;
  }

  public setAttributes(attributes: Attribute[]): void {
    this.attributes = attributes;
  }

  public getAttributes(): Attribute[] {
    return this.attributes;
  }

  public setSuggestions(suggestions: string[]) {
    this.suggestions = suggestions;
  }

  getAttributeSuggestions(searchVal: string) {
    const instructionServer: InstructionServer = InstructionServer.getInstructionServer('', 'AttributeSearch', searchVal);
    this.getAttributeSearchFromServer(instructionServer);
  }

  private getAttributeSearchFromServer(instructionServer: InstructionServer) {
    if (this.userHolder.isLoggedIn()) {
      instructionServer.Email = this.userHolder.login.Email;
    }
    this.socketService.sendRequest(instructionServer, this.userHolder.isLoggedIn() ? this.userHolder.login.Password : '').first().subscribe(instruction => {
      const message: Message = instruction.Argument as Message;
      const attributeSearch: AttributeSearch = message.Argument as AttributeSearch;
      this.updateSuggestions(attributeSearch);
    });
  }

  public addAttribute(attrName: string, attrVal: string): void {
    const tempAttr: Attribute = new Attribute();
    tempAttr.setAttributeData(attrName, attrVal);
    this.getAttributes().push(tempAttr);
    this.getSuggestions();
  }

  getSuggestions(): void {
    const instructionServer = InstructionServer.getInstructionServer('', 'SearchRelated', this.getAttributes());
    if (this.userHolder.isLoggedIn()) {
      instructionServer.Email = this.userHolder.login.Email;
    }

    this.socketService.sendRequest(instructionServer, this.userHolder.isLoggedIn() ? this.userHolder.login.Password : '').first().subscribe(instruction => {
      const message: Message = instruction.Argument as Message;
      const attributes: Attribute[] = message.Argument as Attribute[];
      this.setApriori(attributes);
    });
  }

  getAttributeCounts(): Observable<Map<string, Number>> {
    const subject: Subject<Map<string, Number>> = new Subject();
    const instructionServer = InstructionServer.getInstructionServer(this.userHolder.login.Email, 'GetSearchAttributeCount', this.getSearchAttributes());
    this.socketService.sendRequest(instructionServer, this.userHolder.login.Password).first().subscribe(instruction => {
      const message: Message = instruction.Argument as Message;
      subject.next(message.Argument as Map<string, Number>);
    });
    return subject;
  }

}
