import {User} from './user';

export class Registration {
  public User: Object;

  constructor(u: Object = new User, public Password: string) {
    this.User = u;
  }
}
