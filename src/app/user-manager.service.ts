import {UserHolder} from './user-holder';
import {Injectable} from '@angular/core';
import {User} from './data-objects/user';
import {Attribute} from './data-objects/attribute';
import {UserDTO} from './data-objects/user-dto';
import {Login} from './data-objects/login';
import {InstructionServer} from './instruction-server';
import {Message} from './data-objects/message';
import {ISocketService} from './ISocketService';
import {SecurityService} from './security';
import {AttributeService} from './attribute.service';
import {Observable} from 'rxjs/Observable';
import {Subject} from 'rxjs/Subject';
import {Registration} from './data-objects/Registration';
import {Types} from './data-objects/type';


@Injectable()
export class UserManagerService {

  private userAdded: boolean;
  private isValidatedLogin: boolean;
  private myLocalStorage = localStorage;

  constructor(private socketManagerService: ISocketService, private userHolder: UserHolder,
              private securityService: SecurityService, private attributeService: AttributeService) {
    this.initUser();
  }

  private initUser(): void {
    if (this.myLocalStorage.getItem('user')) {
      try {
        this.userHolder.login = JSON.parse(this.myLocalStorage.getItem('login'));
        const userDTO: UserDTO = JSON.parse(this.myLocalStorage.getItem('user'));
        this.userHolder.currentUser = new User();
        this.userHolder.currentUser.setUser(userDTO);
      } catch (e) {
        console.log('Exception: ' + e);
      }
    }
  }

  public getCurrentUser(): User {
    return this.userHolder.currentUser;
  }

  public getIsValidatedLogin() {
    return this.isValidatedLogin;
  }

  public setUserFromDTO(userDTO: UserDTO): void {
    const user: User = new User();
    user.setUser(userDTO);
    this.setUser(user);
  }

  public setUser(user: User): void {
    this.userAdded = false;
    if (user.Email === '') this.userHolder.currentUser = null;
    this.userHolder.currentUser = user;
    this.saveUser();
  }

  public saveUser(): void {
    this.myLocalStorage.setItem('user', JSON.stringify(this.userHolder.currentUser));
  }

  public getLogin(): Login {
    return this.userHolder.login;
  }

  public setLogin(login: Login) {
    this.userHolder.login = login;
    this.myLocalStorage.setItem('login', JSON.stringify(this.userHolder.login));
  }

  public updateUserAttributes(attributes: Attribute[]): void {
    this.userHolder.currentUser.Attributes = attributes;
  }

  public updateUser(): void {
    const instructionServer = InstructionServer.getInstructionServer(this.getLogin().Email, 'UpdateUser', this.getCurrentUser());
    this.socketManagerService.sendRequest(instructionServer, this.userHolder.login.Password).first().subscribe(instruction => {
      const message: Message = instruction.Argument as Message;
      //TODO feedback til bruger
    });
  }

  public isLoggedIn(): boolean {
    return this.userHolder.currentUser !== null && this.userHolder.currentUser.Name !== '';
  }

  public userLoggedOut(): void {
    this.userHolder.login = null; //TODO måske observable til håndtering af login fejl
    this.userHolder.currentUser = null;
    this.myLocalStorage.clear();
  }

  sendLoginRequest(email: string, password: string): Observable<boolean> {
    const subject: Subject<boolean> = new Subject();
    const login: Login = new Login(email, this.securityService.hashString(password));
    const instructionServer = InstructionServer.getInstructionServer(login.Email, 'Login', login);
    this.setLogin(login);
    this.socketManagerService.sendRequest(instructionServer, login.Password).first().subscribe(instruction => {
      const message: Message = instruction.Argument as Message;
      const user: UserDTO = message.Argument as UserDTO;

      if (user.Email.length === 0) {
        this.userLoggedOut(); // TODO måske feedback
        subject.next(false);
        return;
      }
      this.setUserFromDTO(user);
      this.attributeService.setAttributes(this.getCurrentUser().Attributes);
      this.setLogin(login);
      subject.next(true);
    });
    return subject;
  }

  public signUp(gender: string, name: string, age: number, password: string, email: string): Observable<boolean> {
    const login: Login = new Login(email, password);
    const registrationObject = new Registration(this.createUserObject(this.getAttributesForSignUp(age), name, gender, email), password);
    const instructionServer = InstructionServer.getInstructionServer('RSA', 'CreateUser', registrationObject);
    return this.createUserRequest(instructionServer);
  }

  private createUserRequest(instructionServer: InstructionServer): Observable<boolean> {
    const subject: Subject<boolean> = new Subject();
    this.socketManagerService.sendRequest(instructionServer, 'RSA').first().subscribe(() => {
      subject.next(true);
    });
    return subject;
  }

  private getAttributesForSignUp(age: number): Attribute[] {
    const ageAttribute: Attribute = new Attribute('alder', Types.INTEGER, age);
    this.attributeService.getAttributes().push(ageAttribute);
    return this.attributeService.getAttributes();
  }

  private createUserObject(attributes: Attribute[], name: string, gender: string, email: string): User {
    return new User(name, gender, email, attributes);
  }
}
