import {Attribute} from './attribute';

export class SearchAttribute { //TODO lav navn --> også på sever... PACE

  public Attribute: Attribute; //Samme klassenavn og variabelnavn

  constructor(a: Attribute = new Attribute(), public MinVal: number = -1, public MaxVal: number = -1,
              public Must: boolean = false, public Frequency: number = -1, public Not: boolean = false) {
    this.Attribute = a;
  }
}
