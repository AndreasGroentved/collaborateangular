import {Injectable} from '@angular/core';
import {UserManagerService} from './user-manager.service';
import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot} from '@angular/router';

@Injectable()
export class AuthGuardLoggedOut implements CanActivate {

  constructor(private userManager: UserManagerService) {

  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (this.userManager.isLoggedIn()) {
      // this.router.navigate([''], { queryParams: { returnUrl: state.url }}); //TODO
      return false;
    }
    return true;
  }


}
