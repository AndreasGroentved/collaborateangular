import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from '../login/login.component';
import {SignupComponent} from '../signup/signup.component';
import {LoggedInComponent} from '../logged-in/logged-in.component';
import {AuthGuardLoggedIn} from '../auth-guard-logged-in.service';
import {AuthGuardLoggedOut} from '../auth-guard-logged-out.service';


const appRoutes: Routes = [
  {path: 'login', component: LoginComponent, canActivate: [AuthGuardLoggedOut]},
  {path: 'register', component: SignupComponent, canActivate: [AuthGuardLoggedOut]},
  {path: '', component: LoggedInComponent, pathMatch: 'full', canActivate: [AuthGuardLoggedIn]},
  {path: '**', component: LoggedInComponent, pathMatch: 'full', canActivate: [AuthGuardLoggedIn]},
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {
}
